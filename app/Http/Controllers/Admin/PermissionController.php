<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        $perms = Permission::all();
        return view('admin.permissions.index', compact('perms'));
    }

    public function create()
    {
        $permissions = Permission::all();
        return view('admin.permissions.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        try {
            $permission = Permission::create($request->all());
        } catch (\Error $error) {
            return redirect()->back()->withInput($request->all());
        }
        \Session::flash('success', "Perm $permission->name was created successfully");
        return redirect()->route('perms.index');
    }

    public function edit($id)
    {
        $permission = Permission::where('id', $id)->first();
        $permissions = Permission::all();

//        dd($permission, $permissions);
        return view('admin.permissions.edit', compact('permissions', 'permission'));
    }

    public function update(Request $request)
    {

    }

}
