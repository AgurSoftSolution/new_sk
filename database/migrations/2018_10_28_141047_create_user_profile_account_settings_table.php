<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfileAccountSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_account_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('params');
            $table->string('type')->nullable();
            $table->unsignedInteger('user_id');
            $table->bigInteger('numeric_value')->nullable();
            $table->boolean('bool_value')->nullable();
            $table->mediumText('text_value')->nullable();
            $table->foreign('user_id')->on('id')->references('user_profiles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_account_settings');
    }
}
