<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', '=', 'admin')->first();
        $admin = factory(\App\User::class, 1)->create([
            'email' => 'admin@gmail.com',
            'password' => Hash::make(24049898)
        ]);
        $admin[0]->assignRole($role);
    }
}
