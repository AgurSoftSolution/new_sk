@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Permission Management</h2>
            </div>
            <div class="pull-right">
                @can('perms-create')
                    <a class="btn btn-success" href="{{ route('perms.create') }}"> Create New Permission</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-hover table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($perms as $perm)
            <tr>
                <th>{{$perm->id}}</th>
                <td>{{$perm->name}}</td>
                <td>
                    <a class="btn btn-info" href="{{ route('perms.show',$perm->id) }}">Show</a>
                    @can('perms-edit')
                        <a class="btn btn-warning" href="{{ route('perms.edit',$perm->id) }}">Edit</a>
                    @endcan
                    @can('perms-delete')
                        {!! Form::open(['method' => 'DELETE','route' => ['perms.destroy', $perm->id],'style'=>'display:inline']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>



@endsection
