@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create New Permission</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('perms.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-6">
                {{Form::open(['url' => route('perms.update',$permission->id), 'method' => 'POST'])}}
                @csrf
                <div class="form-group">
                    {{Form::label('name','Name')}}
                    {{Form::text('name',$permission->name??'',['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::submit('Send',['class'=>'btn btn-success'])}}
                </div>
                {{Form::close()}}
            </div>
            <div class="col-6">
                <div class="list-group">
                    @foreach ($permissions as $permission)
                        <a href="#" class="list-group-item list-group-item-action">
                            {{$permission->name}}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
